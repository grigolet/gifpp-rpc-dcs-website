# gifpp-rpc-dcs-website

Website to control the ep-dt-fs rpc

# Steps to build the image

Build the image and tag it with th eigtlab registry name
```
docker build -t gitlab-registry.cern.ch/grigolet/gifpp-rpc-dcs-website .
```
Push it to the gitlab registry
```
docker push gitlab-registry.cern.ch/grigolet/gifpp-rpc-dcs-website
```